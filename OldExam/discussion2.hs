-- Aufgabe 1
-- (b)
countEvens :: [Int] -> Int
countEvens = foldl f 0
    where
        f acc x
            | x `mod` 2 == 0 = acc + 1
            | otherwise = acc

countEvens2 :: [Int] -> Int
countEvens2 = length . filter even

countEvens3 :: [Int] -> Int
countEvens3 [] = 0
countEvens3 (x:xs)
    | even x = 1 + countEvens3 xs
    | otherwise = countEvens3 xs

-- (c)

claim :: Eq b => (a -> b) -> [a] -> [a] -> Bool
claim f x y = map f (x ++ y) == map f x ++ map f y
-- immer True, weil
-- map f ([x1..xn] [y1..yk])
--  = map f [x1..xn, y1..yk]
--  = [f x1,..,f xn, f y1,..,f yk]
--  = [f x1,..,f xn] ++ [f y1,..,f yk]
--  = map f [x1..xn] ++ map f [y1..yk]


data Tree a = Tree a [Tree a]

t1 :: Tree Char
t1 = Tree 'a' []

t2 :: Tree Char
t2 = Tree 'b' [ Tree 'c' [], Tree 'd' []]

t3 :: Tree Char
t3 = Tree 'e' [t1, t2]

t4 :: Tree Char
t4 = Tree 'f' [Tree 'g' [], t3, t2]

depth :: Tree a -> Int
depth (Tree _ []) = 1
depth (Tree _ ts) = 1 + (maximum (map depth ts))

filterMap :: (a -> Maybe b) -> [a] -> [b]
filterMap p [] = []
filterMap p (x:xs) = case p x of
    Nothing -> filterMap p xs
    Just y  -> y : filterMap p xs

g :: Int -> Maybe Int
g x | x <= 3 = Just x
    | otherwise = Nothing

filterMapTR :: (a -> Maybe b) -> [a] -> [b]
filterMapTR p = aux []
    where
        aux acc [] = reverse acc
        aux acc (x:xs) = case p x of
            Nothing -> aux acc xs
            Just y  -> aux (y:acc) xs