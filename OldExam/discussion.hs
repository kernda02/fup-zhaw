
-----------------------------------------------------------
-- Exercise 1
-----------------------------------------------------------
-- (b)
-- (i)
countEvens :: [Int] -> Int
countEvens = foldl f 0
    where
        f acc x
            | x `mod` 2 == 0 = acc + 1
            | otherwise = acc
-- (ii)
countEvens2 :: [Int] -> Int
countEvens2 [] = 0
countEvens2 (x:xs)
    | x `mod` 2 == 0 = 1 + countEvens2 xs
    | otherwise = countEvens2 xs

-- (c)
claim :: Eq b => (a -> b) -> [a] -> [a] -> Bool
claim f x y = map f (x ++ y) == map f x ++ map f y

-- (i) Answer = the first choice

-- (ii)
-- map f ([x1..xn] ++ [y1..ym])
--     = map f [x1..xn,y1..ym]
--     = [f x1,.., f xn, f y1,.., f ym]
--     = [f x1,.., f xn] ++ [f y1,.., f ym]
--     = map f [x1..xn] ++ map f [y1..ym]

-----------------------------------------------------------
-- Exercise 2
-----------------------------------------------------------

data Tree a = Tree a [Tree a]

t1 :: Tree Char
t1 = Tree 'a' []

t2 :: Tree Char
t2 = Tree 'b' [ Tree 'c' [], Tree 'd' []]

t3 :: Tree Char
t3 = Tree 'e' [t1, t2]

t4 :: Tree Char
t4 = Tree 'f' [Tree 'g' [], t3, t2]

-- (ii)
depth :: Tree a -> Int
depth (Tree _ []) = 1
depth (Tree _ ts) = 1 + (maximum (map depth ts))

-----------------------------------------------------------
-- Exercise 3
-----------------------------------------------------------

filterMap :: (a -> Maybe b) -> [a] -> [b]
filterMap p [] = []
filterMap p (x:xs) = case p x of
    Just y -> y : (filterMap p xs)
    Nothing -> filterMap p xs

-- (b)
g :: Int -> Maybe Int
g x | x < 4 = Just x
    | otherwise = Nothing

-- (c)
filterMapTR :: (a -> Maybe b) -> [a] -> [b]
filterMapTR p = aux []
    where
        aux acc [] = reverse acc
        aux acc (x:xs) = case p x of
            Just y -> aux (y:acc) xs
            Nothing -> aux acc xs