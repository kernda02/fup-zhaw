data CardFace
    = Ace
    | King
    | Queen
    | Jack
    | Number Integer
    deriving (Show, Eq)

data CardSuite
    = Club
    | Diamond
    | Heart
    | Spade
    deriving (Show, Eq)

data Card = Card
    { face :: CardFace
    , suite:: CardSuite
    } deriving (Show, Eq)

prettyPrint :: Card -> String
prettyPrint (Card face suite) =
    "[ " ++ prettyFace ++ " | " ++ prettySuite ++ " ]"
    where
      prettyFace :: String
      prettyFace = case face of
        Ace -> "A"
        King -> "K"
        Queen -> "Q"
        Jack -> "J"
        Number x -> show x

      prettySuite :: String
      prettySuite = case suite of
        Club -> "oOo"
        Diamond -> "<>"
        Heart -> "<3"
        Spade -> "o^o"


data CardValue
    = Choice Integer Integer
    | Definite Integer
    deriving (Show, Eq)

value :: Card -> CardValue
value card = case face card of
    Number x -> Definite x
    Ace -> Choice 1 11
    _rest -> Definite 10

data Hand = Hand [Card]
    deriving (Show, Eq)

type HandValue = [Integer]

score :: Hand -> HandValue