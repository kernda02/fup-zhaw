
data Vec a = Vec
    { vLen :: Int
    , vElems :: [a]
    }

data N = Z | S N

data PRFun
    = Const N
    | Proj N
    | Comp Int PRFun [PRFun]
    | PRec Int PRFun PRFun

data Arity
    = Any
    | AtLeast Int
    | Exactly Int
    | Mistyped

allEqual :: Eq a => [a] -> Bool
allEqual = foldl (==) True

arity :: PRFun -> Arity
arity term = case term of
    Const _ -> Any
    Proj n  -> AtLeast n
    Comp f fs ->
        | length fs == arity f && undefined


data Result
    = TypeError String
    | Result N

eval :: PRFun -> [N] -> Result
eval term ns = case term of
    Const n -> Result n
    Proj n
        | n >= length ns -> TypeError "Projection outside of bound"
        | otherwise -> ns !! n
    Comp n f fs
        | n == length fs -> undefined
        | otherwise -> TypeError "Tried to insert wrong number of functions"
