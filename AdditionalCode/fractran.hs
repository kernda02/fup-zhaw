-- Datatype for Fractions
data Fraction = Fraction
    { numerator :: Integer
    , denominator :: Integer
    } deriving (Show, Eq)

-- Possible results of attempting to multiply an Integer with a Fraction.
data MultResult
    = Success Integer
    | Failure
    deriving (Show, Eq)

-- Multiplying Fraction with Integer
(*.) :: Fraction -> Integer -> MultResult
(*.) (Fraction num den) n = case (n * num) `mod` den of
    0 -> Success $ (n * num) `div` den
    _ -> Failure

-- A FRACTRAN program consists of a sequence of fractions
data Program = Program [Fraction]
    deriving (Show, Eq)

-- Executing a FRACTRAN program with an input
execute :: Program -> Integer -> [Integer]
execute (Program p) input = run p [input]
    where
      run :: [Fraction] -> [Integer] -> [Integer]
      run [] inputs = inputs
      run (f:state) (i:inputs) = case f *. i of
        Success r -> run p (r:i:inputs)
        Failure   -> run state (i:inputs)

-- Test

testProgram = Program
    [ Fraction 91 33
    , Fraction 11 13
    , Fraction 1 11
    , Fraction 399 34
    , Fraction 17 19
    , Fraction 1 17
    , Fraction 2 7
    , Fraction 187 5
    , Fraction 1 3
    ]

input = 31250

-- expected 8192
result = head $ execute testProgram input