 * First to install the Gloss library run `stack install gloss`
 * Install System.Random `stack install random`
 * If on windows you might need to copy freeglut.dll to your executable path and rename it to glut32.dll (see here https://stackoverflow.com/questions/42072958/haskell-with-opengl-unknown-glut-entry-glutinit)
 * add the following to C:\sr\global-project\stack.yaml extra-deps:
  [ "JuicyPixels-3.3.1@sha256:315ada189326eb856b6ec244193fafbde44dc12597346ac0c7a4c66643256eaf"
  , "OpenGLRaw-3.3.1.0@sha256:5364ba19a42c02a348be99baa461fc7e6d7908b3da02e0d29cf086730cbc3ac7"
  , "vector-0.12.0.1@sha256:3d644fff350ee3eee2896e4d00cb45e73956f2862f0b18ee502282702682b978"
  , "containers-0.5.11.0@sha256:28ad7337057442f75bc689315ab4ec7bdf5e6b2c39668f306672cecd82c02798"
  ]
* If above does not work, then use the following syntax:
extra-deps:
  - JuicyPixels-3.3.1@sha256:315ada189326eb856b6ec244193fafbde44dc12597346ac0c7a4c66643256eaf
  - OpenGLRaw-3.3.1.0@sha256:5364ba19a42c02a348be99baa461fc7e6d7908b3da02e0d29cf086730cbc3ac7
  - vector-0.12.0.1@sha256:3d644fff350ee3eee2896e4d00cb45e73956f2862f0b18ee502282702682b978
  - containers-0.5.11.0@sha256:28ad7337057442f75bc689315ab4ec7bdf5e6b2c39668f306672cecd82c02798

* Install Gloss export `stack install gloss-export`
* start ghci (Mac): stack ghci --ghci-options -fno-ghci-sandbox