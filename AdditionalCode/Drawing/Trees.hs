module Trees where

import qualified Data.Text as T
import Data.List
import RandomStuff


-----------------------------------------------------------
-- Tree basics
-----------------------------------------------------------

-- Coordinates of a location
type Location = (Float, Float)


-- Every tree needs a DNA to encode its basic
-- morphological properties
data TreeDNA = TreeDNA
    { tdForking :: Int   -- Max number of new (main) branches at a fork.
    , tdCurl    :: Int   -- Max angle in a fork in deg (e.g. 45°)
    , tdStemDom :: Float -- Dominance of the stem (0 = no stem, 1 = dominant stem)
    , tdStocky  :: Float -- Stockyness (0 = extmely thin, 1 = stocky)
    , tdScale   :: Float --
    , tdSizeVar :: Float -- max shrinkage in percent e.g. 20%
    }

data TreeSegment = TreeSegment
    { tsRand  :: [Int]
    , tsPos   :: Location
    , tsAngle :: Float --
    , tsSize  :: Float -- length
    , tsBTL   :: Float
    , tsTTB   :: Float
    }

data TreeSeed = TreeSeed
    { sDNA :: TreeDNA
    , sMutations :: Int
    }

data Tree
    = Leaf TreeDNA TreeSegment
    | Branch TreeDNA TreeSegment [Tree]

-----------------------------------------------------------
-- Growth process
-----------------------------------------------------------

expressAngle :: TreeDNA -> Int -> Float -> Float
expressAngle dna r a0 = a0 + (fromIntegral da)
    where
        mc = tdCurl dna
        da = range r (-mc) mc

expressSize :: TreeDNA -> Int -> Float -> Float
expressSize dna r s0 = rangeF r' lb maxSize
    where
        r' = (fromIntegral $ r `mod` 1000) / 1000
        maxSize = (tdScale dna) * s0
        lb = (100 - (tdSizeVar dna)) * 0.01 * maxSize

sprout :: TreeDNA -> TreeSegment -> TreeSegment
sprout dna env = env
    { tsRand = newRand
    , tsPos = newPos
    , tsAngle = expressAngle dna r1 a
    , tsSize = expressSize dna r2 l
    }
    where
        r1:r2:newRand = tsRand env
        -- new Position
        a = tsAngle env
        l = tsSize env
        dx = 0.95 * l * sin (a * (pi/180))
        dy = 0.95 * l * cos (a * (pi/180))
        (x, y) = tsPos env
        newPos = (x + dx, y + dy)

expressStem :: TreeDNA -> TreeSegment -> [TreeSegment] -> [TreeSegment]
expressStem dna env envs =
    zipWith3 (\s a e -> e {tsAngle = a, tsSize = s}) sizes' angles' envs
    where
        maxSize = (tsSize env) * (tdScale dna)
        (sizes, angles) = unzip [(tsSize e, tsAngle e) | e <- envs ]
        sStem:sSide = reverse $ sort sizes
        aStem:aSide = sortOn (\a -> abs a) angles
        sStem' = sStem + (tdStemDom dna) * (maxSize - sStem)
        aStem' = aStem * (1 - (tdStemDom dna))
        angles' = aStem': aSide
        sizes' = sStem': sSide

branchOut :: TreeDNA -> TreeSegment -> [Tree]
branchOut dna env = map (\e -> Leaf dna e) envs
    where
        r:rs = tsRand env
        nOfTrees = normal rs 1 (tdForking dna)
        envs = expressStem dna env
            $ map (sprout dna)
            $ map (\x -> env {tsRand = x})
            $ split nOfTrees rs

germinate :: Location -> TreeSeed -> Tree
germinate location seed = Leaf dna env
    where
        dna = sDNA seed
        env = TreeSegment
            { tsRand = randoms $ sMutations seed
            , tsPos = location
            , tsAngle = 0
            , tsSize = 100
            , tsBTL = tdStocky dna
            , tsTTB = 0.95 * (tdScale dna)
            }

grow :: Int -> Tree -> Tree
grow 0 t = t
grow n (Leaf dna seg)
    | tsSize seg < 5 = Leaf dna seg
    | otherwise = Branch dna seg
        $ map (grow (n-1))
        $ branchOut dna seg
grow n (Branch dna seg ts) =
    Branch dna seg $ map (grow n) ts
