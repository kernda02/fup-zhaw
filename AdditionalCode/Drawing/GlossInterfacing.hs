module GlossInterfacing where

import qualified Graphics.Gloss as G
import qualified Graphics.Gloss.Export as GE
import Trees

-----------------------------------------------------------
-- Drawing trees
-----------------------------------------------------------

brown :: G.Color
brown = G.makeColorI 80 60 5 255

green :: G.Color
green = G.makeColorI 48 114 76 255

data TypedSegment
    = TSTrunk TreeSegment
    | TSLeaf TreeSegment

drawSegment :: TypedSegment -> G.Picture
drawSegment (TSTrunk seg) =
    G.translate x y
    $ G.rotate angle
    $ G.color brown
    $ G.polygon
        [ ((-0.5) * base, 0)
        , ((-0.5) * top, len)
        , (0.5 * top, len)
        , (0.5 * base, 0)
        ]
    where
        (x, y) = tsPos seg
        len = tsSize seg
        angle = (tsAngle seg)
        base = (tsBTL seg) * len
        top  = (tsTTB seg) * base

drawSegment (TSLeaf seg) = G.translate x y
    $ G.rotate angle
    $ G.color green
    $ G.Polygon
        [ (0,0)
        , (-width ,len * 0.5)
        , (0, len)
        , (width, len * 0.5)
         ]
    where
        (x, y) = tsPos seg
        len = tsSize seg
        width = 0.3 * len
        angle = (tsAngle seg)

drawTree :: Tree -> G.Picture
drawTree tree = G.pictures
    $ map drawSegment
    $ segments tree
    where
        segments (Leaf _ env) = [TSLeaf env]
        segments (Branch _ env ts) = (TSTrunk env) : (concatMap segments ts)

treeToPNG :: Tree -> IO()
treeToPNG tree =
    GE.exportPictureToPNG
        (2000,1000)
        G.white
        "myTree.png"
        (drawTree tree)

treesToPNG :: Float -> [Tree] -> IO()
treesToPNG offset ts = GE.exportPictureToPNG (2500,1000) G.white "myTrees.png"
    $ G.pictures
    $ map drawTree
    $ map (\(n, t) -> moveTree (n*offset) 0 t)
    $ zip [0..] ts


-----------------------------------------------------------
-- Auxiliary functions
-----------------------------------------------------------

handleTree :: (TreeSegment -> TreeSegment) -> Tree -> Tree
handleTree f (Leaf dna seg) = Leaf dna $ f seg
handleTree f (Branch dna seg trees) = Branch dna (f seg)
    $ map (handleTree f) trees

moveTree :: Float -> Float -> Tree -> Tree
moveTree dx dy = handleTree mv
   where
       mv seg@(TreeSegment _ (x,y) _ _ _ _) =
           seg { tsPos = (x+dx, y+dy) }