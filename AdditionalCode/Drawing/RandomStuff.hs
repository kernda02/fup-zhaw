module RandomStuff where

import qualified System.Random as R

gen :: Int -> R.StdGen
gen x = R.mkStdGen x

-- Generates a sequence of random numbers from a given seed
randoms :: Int -> [Int]
randoms = R.randoms . gen

-- Meant to split a (infinite) sequence of random numbers into multiple (n)
-- sequences of random numbers.
split :: Int -> [Int] -> [[Int]]
split n xs = take n $ map randoms xs

-- Given some (random) number seed and a lower bound lb as well as an upper
-- bound ub, this function provides a (random) number r s.t. lb <= r <= ub
range :: Int -> Int -> Int -> Int
range seed lb ub = lb + (seed `mod` ((ub + 1) - lb))

rangeF :: Float -> Float -> Float -> Float
rangeF seed lb ub = lb + seed * (ub - lb)

-- Does the same as range, but with a bias towards the upper bound.
biasedRange :: Int -> Int -> Int -> Int
biasedRange seed lb ub = range seed (range seed lb ub) ub

-- 'Normal' distribution
normal :: [Int] -> Int -> Int -> Int
normal seeds lb ub =
    s `div` 6
    where
        s = sum $ map (\x -> range x lb ub) $ take 6 seeds
