
import Trees
import GlossInterfacing
import qualified Graphics.Gloss as G

seed1 y = TreeSeed
    { sDNA = TreeDNA
        { tdForking = 7
        , tdCurl = 75
        , tdStemDom = 0.8
        , tdStocky = 0.2
        , tdScale = 0.92
        , tdSizeVar = 75
        }
    , sMutations = y
    }

seed2 y = seed
    { sDNA = (sDNA seed) { tdStemDom = 0 }
    }
    where
        seed = seed1 y

tree1 x y = grow x $ germinate (-500,-500) $ seed1 y
tree2 x y = grow x $ germinate (-500,-500) $ seed2 y


compareTrees x y = treesToPNG 600 [tree1 x y, tree2 x y]
