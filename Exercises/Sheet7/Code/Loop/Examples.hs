module Examples where

import Syntax
import Semantics

-------------------------------------------------------------------------------
-- | Some example Loop programs
-------------------------------------------------------------------------------

-- | Example programs 'p' can be run with input 'x1..xn'
-- | by evaluating 'run p [x1..xn]'

-- | Addition
-- | x0 = x1 + x2
pAdd :: Program
pAdd = Assignment 0 (Plus (X 1) (X 2))

-- | Multiplication
-- | Loop x1 Do
-- |    x0 = x0 + x2
-- | End
pMul :: Program
pMul = Loop (X 1) (Assignment 0 (Plus (X 0) (X 2)))

-- | Modulo
-- | x0 = x1 + 0;
-- | LOOP x1 DO
-- |  x1 = x1 - x2;
-- |  x3 = x1 + 0;
-- |  IF x3 > 0 THEN x0 = x1 + 0
-- | END;
-- | x1 = x2 - 1;
-- | x1 = x0 - x1;
-- | LOOP x1 DO
-- |  x0 = 0 + 0
-- | END
pMod :: Program
pMod = seqL
    [ Assignment 0 (Plus (X 1) (C 0))
    , Loop (X 1) $
        seqL
            [ Assignment 1 (Minus (X 1) (X 2))
            , Assignment 3 (Plus (X 1) (C 0))
            , ifThen 3 (Assignment 0 (Plus (X 1) (C 0)))
            ]
    , Assignment 1 (Minus (X 2) (C 1))
    , Assignment 1 (Minus (X 0) (X 1))
    , Loop (X 1) $
        Assignment 0 (Plus (C 0) (C 0))
    ]

-- | Max
pMax :: Program
pMax = seqL
    [ Assignment 3 (Minus (X 1) (X 2))
    , Assignment 4 (Minus (X 2) (X 1))
    , ifThen 3 (Assignment 0 (Plus (C 0) (X 1)))
    , ifThen 4 (Assignment 0 (Plus (C 0) (X 2)))
    ]


