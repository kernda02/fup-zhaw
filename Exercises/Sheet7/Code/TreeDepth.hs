data Tree a
    = Leaf a
    | Node a (Tree a) (Tree a)

depthAcc :: Int -> [Tree a] -> Int
depthAcc n [] = n
depthAcc n ts =
    depthAcc (n+1) $ concatMap flatten ts
    where
        flatten (Leaf _) = []
        flatten (Node _ t1 t2) = [t1, t2]

depth :: Tree a -> Int
depth t = depthAcc 0 [t]

tree :: Tree Int
tree = Node 2 (Leaf 2) (Node 3 (Leaf 4) (Leaf 5))

tree2 = Node 1 tree tree
