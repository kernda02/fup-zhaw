
-- | Exercise 1 & 2
square :: Integer -> Integer
square n = n * n

exp3 :: Integer -> Integer
exp3 0 = 1
exp3 n = 3 * exp3 (n-1)

makePalindrome :: String -> String
makePalindrome w = w ++ (reverse w)

summe :: (Integer -> Integer) -> Integer -> Integer -> Integer
summe f n m = foldl (+) 0 [f i | i <- [n..m]]

sumSq :: Integer -> Integer -> Integer
sumSq = summe square

-- | Exercise 3

substring :: String -> String -> Bool
substring s w =
    ls <= lw && (take ls w == s || substring s (tail w))
    where
      ls = length s
      lw = length w

-- | Exercise 4

-- Replaces all ocurrences of the first string (as a substring of the third
-- parameter by the second parameter.
replace :: String -> String -> String -> String
replace pattern newPattern str
    | lp == 0 = str -- Nothing to replace
    | lp > ls = str -- Nothing to replace
    | take lp str == pattern = -- Here replacement happens
        newPattern ++ recurse (drop lp str)
    | otherwise = head str : recurse (tail str) -- Nothing to replace but maybe later on
    where
      lp = length pattern
      ls = length str
      recurse = replace pattern newPattern


-- Slightly more general and tail recursive version
replace' :: (String -> String) -> String -> String -> String -> String
replace' transform pattern acc "" = acc
replace' transform pattern acc str
    | pattern == "" = str
    | lp > ls = acc ++ str
    | take lp str == pattern =
        replace' transform pattern (acc ++ transformed) (drop lp str)
    | otherwise = replace' transform pattern (acc ++ [head str]) (tail str)
    where
      lp = length pattern
      ls = length str
      transformed = transform pattern

parens :: String -> String
parens = replace "a" "<a>"

parens2 :: String -> String
parens2 = replace "abc" "<abc>"