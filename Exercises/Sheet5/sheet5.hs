newtype Boxed a = Boxed {unbox :: a} deriving Show

instance Functor Boxed where
    fmap f (Boxed a) = Boxed (f a)

newtype FromInt b = FromInt {fun :: Int -> b}
