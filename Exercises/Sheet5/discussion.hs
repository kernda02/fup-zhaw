

covRec :: ([Integer] -> Integer) -> Integer -> Integer
covRec g n = g $ map f [0..(n-1)]
    where
        f = covRec g

covRecRev :: ([Integer] -> Integer) -> Integer -> Integer
covRecRev g n = g [f (n-i) | i <- [1..n]]
    where
        f = covRecRev g

fibs :: Integer -> Integer
fibs = covRec (g . reverse)
    where
        g []  = 1
        g [x] = 1
        g (x:y:_) = x + y


fibR :: Integer -> Integer
fibR = covRecRev g
    where
        g []  = 1
        g [_] = 1
        g (x:y:_) = x + y
