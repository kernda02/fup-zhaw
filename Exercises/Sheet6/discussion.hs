-----------------------------------------------------------
-- Aufgabe 1
-----------------------------------------------------------
-- Endrekursiv ist nur length'

-----------------------------------------------------------
-- Aufgabe 2
-----------------------------------------------------------

-- | Das Original
sieve :: (a -> a -> Bool) -> [a] -> [a]
sieve pred xs = case xs of
   [] -> []
   x:xs -> x:(sieve pred $ filter (pred x) xs)

-- | Mit Akkumulator
sieveAcc :: (a -> a -> Bool) -> [a] -> [a]
sieveAcc pred = sieve' []
    where
        sieve' acc [] = reverse acc
        sieve' acc (x:xs) = sieve' (x:acc) $ filter (pred x) xs

-- | Mit Continuation
sieveCont :: (a -> a -> Bool) -> [a] -> [a]
sieveCont pred = sieve' id
    where
        sieve' f [] = f []
        sieve' f (x:xs) =
            sieve' (\ys -> f (x:ys)) $ filter (pred x) xs

-----------------------------------------------------------
-- Aufgabe 3
-----------------------------------------------------------

-- | Ohne Fixpunkte ginge es so
primesN :: [Integer] -> [Integer]
primesN [] = []
primesN (x:xs) = x:(filter (\y -> y `mod` x /= 0) (primesN xs) )

-- | Der Fixpunktkombinator
fix f = f (fix f)


-- | Fast
primesF :: ([Integer] -> [Integer]) -> [Integer] -> [Integer]
primesF _ [] = []
primesF f (x:xs) = x:(filter (\y -> y `mod` x /= 0) (f xs))

-- | Richtig
primesF2 f n = case n of
    0 -> []
    1 -> []
    2 -> [2]
    _
     | prime     -> n:current
     | otherwise -> current
    where
        current = f (n-1)
        prime = all (\x -> n `mod` x /= 0) current
