-- | Aufgabe 1
{-
(a) Der Typ ist Integer -> Integer
(b) Wird die Deklaration y :: Integer weggelassen,
so kann ein allgemeinerer Typ verwendet werden:
add13 :: Num a => a -> a
dieser Typ besagt, dass als Argument jeder Wert
eingesetzt werden kann, der von einem mit dem
"gerechnet werden kann" (der Typ hat eine 'Num'
instanz). Dies ist noetig, damit die Funktion (+)
angewendet werden kann.
-}

-- | Aufgabe 2
-- (a)
negate1 :: Bool -> Bool
negate1 True  = False
negate1 False = True

-- (b)
negate2 :: Bool -> Bool
negate2 x = case x of
    True -> False
    False -> True

-- (c)
negate3 :: Bool -> Bool
negate3 x
    | x         = False
    | otherwise = True

-- (d)
negate4 :: Bool -> Bool
negate4 x = if x then False else True

-- | Aufgabe 3
isEven3 :: Integer -> Bool
isEven3 n
    | n `mod` 2 == 0 = True
    | otherwise      = False

isEven4 :: Integer -> Bool
isEven4 n =
    if n `mod` 2 == 0 then
        True
    else
        False

-- | Aufgabe 4
-- (a)
negateL :: Bool -> Bool
negateL = \x -> if x then False else True

--(b)
add1 :: Integer -> Integer -> Integer
add1 x = \y -> x + y

add2 :: Integer -> Integer -> Integer
add2 = \x y -> x + y

add3 :: Integer -> Integer -> Integer
add3 = \x -> (\y -> x + y)

-- | Aufgabe 5
-- (a)
seq2 :: (a -> b) -> (b -> c) -> a -> c
seq2 f g x = g $ f x

-- (b)
isOdd :: Integer -> Bool
isOdd = seq2 isEven3 negate3

-- (c)
seqMany :: [a -> a] -> a -> a
seqMany [] x = x
seqMany (f:fs) x = seqMany fs $ f x

-- (d)
seqManyL :: [a -> a] -> a -> a
seqManyL = foldl seq2 id

seqManyR :: [a -> a] -> a -> a
seqManyR = foldr seq2 id