data MBTree a
    = Leaf a
    | MBTree (MBTree a) (Maybe a) (MBTree a)

mbTree :: (a -> b) -> (b -> Maybe a -> b -> b) -> MBTree a -> b
mbTree leaf mbt (Leaf a) = leaf a
mbTree leaf mbt (MBTree tl ma tr) = mbt (recurse tl) ma (recurse tr)
    where
        recurse = mbTree leaf mbt

flatten :: MBTree a -> [a]
flatten = mbTree leaf mbt
    where
        leaf x = [x]
        mbt ls Nothing rl = ls ++ rl
        mbt la (Just x) rl = la ++ (x:rl)


toTex :: MBTree String -> String
toTex = mbTree leaf mbt
    where
        leaf x = "[" ++ x ++ "]"
        mbt ls Nothing rs = "[" ++ ls ++ rs ++ "]"
        mbt ls (Just x) rs = "[" ++ x ++ ls ++ rs ++ "]"

t1 = MBTree (Leaf "2") (Just "1") (MBTree (Leaf "3") Nothing (Leaf "4"))

-- some random discussion
data Term
    = C Int
    | Sum Term Term
    | Mul Term Term

term :: (Int -> a) -> (a -> a -> a) -> (a -> a -> a) -> Term -> a
term c _ _ (C i) = c i
term c s m (Sum t1 t2) = s (term c s m t1) (term c s m t2)
term c s m (Mul t1 t2) = m (term c s m t1) (term c s m t2)



exTerm =
    Mul
        (C 2)
        (Sum
            (Sum
                (C 2)
                (C 3)
            )
            (C 4)
        )

eval :: Term -> Int
eval = term id (+) (*)

