data MBTree a = Leaf a | MBTree (MBTree a) (Maybe a) (MBTree a)

data Tree a = Tree a [Tree a] deriving Show

tree :: (a -> [b] -> b) -> Tree a -> b
tree treeC (Tree a ts) =
    treeC a $ map (tree treeC) ts

weight :: Num a => Tree a -> a
weight = tree treeC
    where
        treeC a as = a + sum as

depth :: Tree a -> Int
depth = tree treeC
    where
        treeC a [] = 1
        treeC a ns = 1 + maximum ns

treeMap :: (a -> b) -> Tree a -> Tree b
treeMap f = tree (\a -> Tree (f a))

instance Functor Tree where
    fmap = treeMap










