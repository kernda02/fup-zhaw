import Data.List

-----------------------------------------------------------
-- Exercise 1
-----------------------------------------------------------


data BTree a
    = Leaf a
    | BNode (BTree a) a (BTree a)
    deriving (Show, Eq)

-----------------------------------------------------------
-- a)
collect :: BTree a -> [a]
collect (Leaf a) = [a]
collect (BNode left a right) =
    (collect left)
    ++ [a]
    ++ (collect right)

-- Example
--   1
--  / \
-- 2   3
bTree :: BTree Integer
bTree = BNode (Leaf 2) 1 (Leaf 3)

-----------------------------------------------------------
-- b)
data Tree a = Node a [Tree a]
    deriving (Show, Eq)

-- Example
--    1
--  / | \
-- 2  3  4
--      / \
--     5   6
tree :: Tree Integer
tree =
    Node 1
      [ Node 2 []
      , Node 3 []
      , Node 4
        [ Node 5 []
        , Node 6 []
        ]
      ]

-----------------------------------------------------------
-- Exercise 2
-----------------------------------------------------------

data NatNumber = Zero | Succ NatNumber
    deriving (Show, Eq)

eval :: NatNumber -> Integer
eval Zero = 0
eval (Succ n) = 1 + eval n

interpret :: Integer -> NatNumber
interpret 0 = Zero
interpret n = Succ $ interpret (n - 1)

-- Bootrstapping of addition and multiplication
add :: NatNumber -> NatNumber -> NatNumber
add n Zero = n
add n (Succ m) = add (Succ n) m

mul :: NatNumber -> NatNumber -> NatNumber
mul n Zero = Zero
mul n (Succ m) = (mul n m) `add` n

-- Use addition and multiplication to define factorial
fact :: NatNumber -> NatNumber
fact Zero = Succ Zero
fact (Succ n) = Succ n `mul` fact n

-----------------------------------------------------------
-- Exercise 3
-----------------------------------------------------------
-- Available in section "additional code"


-----------------------------------------------------------
-- Exercise 4
-----------------------------------------------------------
-- (a)
newtype Model = Model String
    deriving (Show, Eq)

newtype Make = Make String
    deriving (Show, Eq)

-- | RGB colors
data Color = Color
    { red :: Int
    , green :: Int
    , blue :: Int
    } deriving (Show, Eq)

-- | Some simple colors
redColor = Color 255 0 0
greenColor = Color 0 255 0
blueColor = Color 0 0 255

-- | Horsepower
newtype HP = HP Int
    deriving (Show, Eq, Ord)

data Car = Car
    { model :: Model
    , make :: Make
    , year :: Integer
    , color :: Color
    , power :: HP
    } deriving (Show, Eq)

-- (b)

ford :: Car
ford = Car
    { model = Model "Fiesta"
    , make = Make "Ford"
    , year = 2017
    , color = redColor
    , power = HP 70
    }

ferrari :: Car
ferrari = Car
    { model = Model "Testarossa"
    , make = Make "Ferrari"
    , year = 1998
    , color = greenColor
    , power = HP 470
    }

zoe :: Car
zoe = Car
    { model = Model "Zoe"
    , make = Make "Renault"
    , year = 2019
    , color = Color 255 255 255
    , power = HP 110
    }

-- (c)
instance Ord Car where
    (<=) car1 car2 = (power car1) <= (power car2)

-- (d)
sortedCars = sort [zoe, ferrari, ford]