-----------------------------------------------------------
-- First session
-----------------------------------------------------------


-- 1)

data Tree a
    = Node (Tree a) a (Tree a)
    | Leaf a

collect :: Tree a -> [a]
collect (Leaf a) = [a]
collect (Node left a right) = concat
    [ collect left
    , [a]
    , collect right
    ]

t = Node (Leaf 2) 1 (Leaf 3)
t2 = Node t 12 t

-- 2)

data Nat
    = Zero
    | Succ Nat
    deriving (Show, Eq)

eval :: Nat -> Integer
eval Zero = 0
eval (Succ n) = 1 + (eval n)

interpret :: Integer -> Nat
interpret n
    | n <= 0 = Zero
    | otherwise = Succ $ interpret (n - 1)

(+.) :: Nat -> Nat -> Nat
(+.) Zero m = m
(+.) (Succ n) m = n +. (Succ m)

(*.) :: Nat -> Nat -> Nat
(*.) Zero n = Zero
(*.) (Succ n) m = (n *. m) +. m

fak :: Nat -> Nat
fak Zero = Succ Zero
fak (Succ n) = Succ n *. fak n


-----------------------------------------------------------
-- Second session
-----------------------------------------------------------

data Fraction = Fraction
    { num :: Int
    , denom :: Int
    } deriving (Show, Eq)

type Program = [Fraction]

mul :: Int -> Fraction -> Maybe Int
mul n (Fraction a b)
    | (n * a) `mod` b == 0 = Just $ (n * a) `div` b
    | otherwise = Nothing


testProgram =
    [ Fraction 91 33
    , Fraction 11 13
    , Fraction 1 11
    , Fraction 399 34
    , Fraction 17 19
    , Fraction 1 17
    , Fraction 2 7
    , Fraction 187 5
    , Fraction 1 3
    ]

exec :: Program -> Int -> [Int]
exec p n = run p [n]
    where
        run :: Program -> [Int] -> [Int]
        run [] ns = ns
        run (f:fs) (n:ns) = case n `mul` f of
            Nothing -> run fs (n:ns)
            Just m -> run p (m:n:ns)


