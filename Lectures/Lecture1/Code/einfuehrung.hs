summe :: [Integer] -> Integer
summe [] = 0
summe (a:rest) = a + summe rest

exp :: Integer -> Integer
exp 0 = 1
exp n = 3 * exp (n - 1)

greetings :: String
greetings = "hello world!"

square x = x * x

aNumber :: Integer
aNumber = 4

f x = x ++ "abc"

----------------------------

g :: Integer -> Integer
g x =
  let
    sign
        | x < 0 = -1
        | x > 0 = 1
        | otherwise = 0
  in
    (x * x) * sign

g' :: Integer -> Integer
g' x = (x * x) * sign
  where
    sign
        | x < 0 = -1
        | x > 0 = 1
        | otherwise = 0


---------------------------

fibo :: Integer -> Integer
fibo 0 = 1
fibo 1 = 1
fibo n = fibo (n-1) + fibo (n-2)

fibo' :: Integer -> Integer
fibo' n = case n of
  0 -> 1
  1 -> 1
  _ -> fibo (n-1) + fibo (n-2)

--------------------------------

compose :: (b -> c) -> (a -> b) -> (a -> c)
compose f g x = f (g x)

--------------------------------

(-.-) :: Integer -> Integer -> Integer
(-.-) x y = (x * y) + 5

o :: Integer -> Integer -> Integer
o x y = (x * y) + 5