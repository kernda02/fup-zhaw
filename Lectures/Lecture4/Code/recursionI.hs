-------------------------------------------------------------------------------
-- Natural numbers
-------------------------------------------------------------------------------

data Nat = Z | S Nat

asInt :: Nat -> Integer
asInt Z = 0
asInt (S n) = 1 + (asInt n)

fromInt :: Int -> Nat
fromInt 0 = Z
fromInt n = S $ fromInt $ n-1

-------------------------------------------------------------------------------
-- Primitive recursion schema
-------------------------------------------------------------------------------

primRec 
    :: (Nat -> Nat -> tuple -> Nat) 
    -> (tuple -> Nat) 
    -> Nat 
    -> tuple 
    -> Nat
primRec g c Z x = c x
primRec g c (S n) x = g (f n x) n x
    where
        f = primRec g c

add :: Nat -> Nat -> Nat
add n m = primRec g c n m
    where
        c = id
        g y _ _ = S y 

mul :: Nat -> Nat -> Nat
mul n m = primRec g c n m
    where
        c _ = Z
        g y _ k = y `add` k  

exp2 :: Nat -> Nat
exp2 n = primRec g c n ()
    where
        two = S $ S Z
        g y _ _ = two `mul` y
        c () = S Z

fact :: Nat -> Nat
fact n = primRec g c n ()
    where
        c () = S Z
        g y k _ = (S k) `mul` y

-------------------------------------------------------------------------------
-- Course of value recursion schema
-------------------------------------------------------------------------------

ord :: Nat -> [Nat]
ord = ord' []
    where
        ord' acc Z = acc
        ord' acc (S k) = ord' (k:acc) k

covRec :: ([Nat] -> Nat) -> Nat -> Nat
covRec g Z = g []
covRec g (S n) = g fs
    where
        f = covRec g
        fs = map f $ ord $ S n

pCovRec :: ([Nat] -> Nat -> tuple -> Nat) -> Nat -> tuple -> Nat
pCovRec g Z x = g [] Z x
pCovRec g (S n) x = g fs n x
    where
        f n' x' = pCovRec g n' x' 
        fs = map (\k -> f k x) $ ord $ S n

fibs :: Nat -> Nat
fibs n = pCovRec g n ()
    where
        g [] _ _ = Z
        g [_] _ _ = S Z
        g ns _ _ = case reverse ns of
            (k:m:_) -> k `add` m

-------------------------------------------------------------------------------
-- General recursion
-------------------------------------------------------------------------------

next :: Integer -> Integer
next n
    | n < 2 = 1
    | n `mod` 2 == 0 = n `div` 2
    | otherwise = 3 * n + 1

colLength :: Integer -> Integer
colLength n 
    | n < 2 = 1
    | otherwise = 1 + colLength (next n)

length' :: [a] -> Integer
length' [] = 0
length' (_:xs) = 1 + length' xs 