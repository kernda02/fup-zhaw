
type Function = Integer -> Integer
type Functional = Function -> Function

expF :: Functional
expF f x
    | x == 0 = 1
    | otherwise = 2 * f (x - 1)

e :: Function
e x = undefined

ee :: Function
ee = expF e

eee :: Function
eee = expF ee

-- Works for expF...

iterF :: Integer -> Functional -> Function
iterF 0 _ = e
iterF n fn = fn $ iterF (n-1) fn

fix_ :: Functional -> Function
fix_ fn n = iterF (n+1) fn n

exp_ :: Function
exp_ = fix_ expF

-- But not in general for e.g.:

colF :: Functional
colF f x
    | x == 1 = 0
    | otherwise = 1 + f (next x)
    where
        next y
            | y `mod` 2 == 0 = y `div` 2
            | otherwise = 3 * y + 1

col :: Function
col = fix_ colF

-- We don't have to care about the end/base of the recursion
-- as long as the functional does!

fix :: Functional -> Function
fix fn = fn $ fix fn
--       ^^ functional takes care of the base case!

colTot :: Function
colTot = fix colF

-- Fixed-Points and side-effects

-- "cheating"
-- Recursive memoization
fibF :: Functional
fibF f n
    | n < 2 = n
    | otherwise = f (n-1) + f (n-2)

fib = fix fibF

memo :: Function -> Function
memo f = ([f i | i <- [0..]] !!) . fromIntegral

memoF :: Functional -> Functional
memoF fn f = ([fn f i | i <- [0..]] !!) . fromIntegral

-------------------------------------------------------------------------------
-- Tail calls
-------------------------------------------------------------------------------

sum_ :: [Integer] -> Integer
sum_ [] = 0
sum_ (x:xs) = x + (sum_ xs)

sumTR_ :: Integer -> [Integer] -> Integer
sumTR_ acc [] = acc
sumTR_ acc (x:xs) = sumTR_ (x + acc) xs

sumTR = sumTR_ 0

fakTR :: Integer -> Integer
fakTR = fakTR_ 1
    where
        fakTR_ :: Integer -> Integer -> Integer
        fakTR_ acc 0 = acc
        fakTR_ acc n = fakTR_ (n * acc) (n-1)

pow :: Integer -> Integer -> Integer
pow x = powTR 1
    where
        powTR :: Integer -> Integer -> Integer
        powTR acc 0 = acc
        powTR acc y = powTR (x*acc) (y-1)

isPalindrome :: String -> Bool
isPalindrome w
    | l < 2 = True
    | otherwise =
        w0 == wE && isPalindrome w'
    where
        l = length w
        w0 = head w
        wE = last w
        w' = tail $ init w

isPalTR:: String -> Bool
isPalTR ""  = True
isPalTR [_] = True
isPalTR s
    | head s == last s =
        isPalTR $ tail $ init s
    | otherwise = False

fibTR :: Integer -> Integer
fibTR = f 1 0
    where
        f x y n
            | n <= 1 = x
            | otherwise = f (x + y) x (n-1)

fakC :: Integer -> Integer
fakC = fakC_ (const 1)
    where
        fakC_ f n
            | n < 1 = f n
            | otherwise = fakC_ (\x -> n * (f x)) $ n - 1


myMap :: (a -> b) -> [a] -> [b]
myMap f [] = []
myMap f (x:xs) = (f x):(myMap f xs)

myMapC :: (a -> b) -> [a] -> [b]
myMapC f = mmc id
    where
        mmc g [] = g []
        mmc g (x:xs) = mmc (\as -> g ((f x):as)) xs

-- TreeMap Endrekursiv

data Tree a
    = Leaf a
    | Tree (Tree a) (Tree a)

tSum :: Num a => Tree a -> a
tSum (Leaf a) = a
tSum (Tree l r) = tSum l + tSum r

tSumTR :: Num a => Tree a -> a
tSumTR t = tSumC (const 0) [t]
    where
        tSumC g [] = g ()
        tSumC g (t:ts) = case t of
            Tree l r -> tSumC g (l:r:ts)
            Leaf a -> tSumC g' ts
                where
                    g' x = a + (g x)

t1 = Tree (Tree (Leaf 1) (Leaf 2)) (Leaf 3)
t2 = Tree t1 (Tree t1 t1)