data AExp 
    = Const Integer
    | Div AExp AExp

eval :: AExp -> Integer
eval (Const x) = x
eval (Div e1 e2) = eval e1 `div` eval e2

exp1 :: AExp
exp1 = Div (Const 10) (Const 3)

exp2 :: AExp
exp2 = Div (Const 300) (Div (Const 2) (Const 8))

data Result a
    = Error
    | Result a
    deriving Show 

evalE :: AExp -> Result Integer
evalE (Const x) = Result x
evalE (Div e1 e2) = case evalE e2 of
    Result re2 
        | re2 == 0 -> Error
        | otherwise -> case evalE e1 of
            Result re1 -> Result $ re1 `div` re2
            Error -> Error
    Error -> Error

instance Functor Result where
    fmap f (Result x) = Result $ f x
    fmap _ Error = Error

instance Applicative Result where
    pure = Result
    (Result f) <*> (Result x) = Result $ f x
    _ <*> _ = Error

instance Monad Result where
    Result x >>= f = f x
    Error >>= _    = Error

evalM :: AExp -> Result Integer
evalM (Const x) = Result x
evalM (Div e1 e2) =
    evalM e1 >>= \i1 ->
        evalM e2 >>= \i2 ->
            if i2 == 0 then Error
            else Result $ i1 `div` i2

evalDo :: AExp -> Result Integer
evalDo (Const x)   = Result x
evalDo (Div e1 e2) = do
    i1 <- evalDo e1
    i2 <- evalDo e2
    if i2 == 0 then Error 
    else Result $ i1 `div` i2