
-- Summentypen

data Shape
    = Rectangle Float Float
    | Square Float
    | Circle Float
    deriving (Show, Eq)

area :: Shape -> Float
area (Rectangle a b) = a * b
area (Square a) = a * a
area (Circle r) = 3.14 * r * r

data Tree a
    = Node (Tree a) a (Tree a)
    | Leaf a
    deriving (Show, Eq)

depth :: Tree a -> Integer
depth (Node l _ r) = 1 + max (depth l) (depth r)
depth (Leaf _) = 1

-- data CardFace
--     = ...
--
-- data CardSuite
--     = ...
--
-- data Card = Card
--     { face :: CardFace
--     , suite:: CardSuite
--     } deriving (Show, Eq)


-- Listen
liste = [i + 2 | i <- [1..5], i `mod` 2 == 0]


-- Klassen
data Person = Person
    { name :: String
    , age :: Integer
    , idNumber :: Integer
    }

instance Eq Person where
    (==) (Person _ _ id1) (Person _ _ id2) = id1 == id2

hans = Person "Hans" 34 123
anna = Person "Anna" 27 124