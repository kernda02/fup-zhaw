
-------------------------------------------------------------------------------
-- Functor
-------------------------------------------------------------------------------

predec :: Int -> Maybe Int
predec x
    | x <= 1    = Nothing
    | otherwise = Just $ x - 1

divS :: Int -> Int -> Maybe Int
divS x y | y == 0    = Nothing
        | otherwise = Just $ x `div` y

f :: Int -> Int
f x = 2 * x

g :: Int -> Maybe Int
g x = f <$> predec x

-------------------------------------------------------------------------------
-- Applicative
-------------------------------------------------------------------------------

h :: Int -> Int -> Maybe Int
h x y = (*) <$> predec x <*> predec y

drei :: Int -> Int -> Int -> Int
drei x y z = x + y + z

d :: Int -> Int -> Int -> Maybe Int
d x y z = drei <$> predec x <*> predec y <*> predec z

data User = User
    { uName  :: String
    , uEmail :: String
    , uCity  :: String
    } deriving Show

type Profile = [(String, String)]

petersProfile :: Profile
petersProfile =
    [ ("name", "peter")
    , ("email", "peter@peter.com")
    , ("city", "zueri")
    ]

incompleteProfile :: Profile
incompleteProfile =
    [ ("name", "Hans")
    , ("emaill", "hans@abc.com")
    , ("city", "zueri")
    ]

myLookup :: String -> Profile -> Maybe String
myLookup str [] = Nothing
myLookup str ((key, value):assocs)
    | str == key = Just value
    | otherwise = myLookup str assocs

buildUser :: Profile -> Maybe User
buildUser profile =
    case myLookup "name" profile of
        Nothing -> Nothing
        Just name -> case myLookup "email" profile of
            Nothing -> Nothing
            Just email -> case myLookup "city" profile of
                Nothing -> Nothing
                Just city -> Just $ User name email city

peter :: Maybe User
peter = buildUser petersProfile

buildUserAp :: Profile -> Maybe User
buildUserAp profile = User
    <$> myLookup "name" profile
    <*> myLookup "email" profile
    <*> myLookup "city" profile

-------------------------------------------------------------------------------
-- Monad
-------------------------------------------------------------------------------
type CityBase = [(String, String)]

buildUserC :: Profile -> CityBase -> Maybe User
buildUserC profile cities =
    case myLookup "name" profile of
        Nothing -> Nothing
        Just name -> case myLookup "email" profile of
            Nothing -> Nothing
            Just email -> case myLookup email cities of
                Nothing -> Nothing
                Just city -> Just $
                    User name email city

buildUserCM :: Profile -> CityBase -> Maybe User
buildUserCM profile cities = do
    name  <- myLookup "name" profile
    email <- myLookup "email" profile
    city  <- myLookup email cities
    pure $ User name email city

buildUserB :: Profile -> CityBase -> Maybe User
buildUserB profile cities =
    myLookup "name" profile
        >>= \n -> myLookup "email" profile
            >>= \e -> myLookup e cities
                >>= \c -> pure $ User n e c

annasProfile :: Profile
annasProfile =
    [ ("name", "Anna")
    , ("email", "anna@nasa.gov")
    ]

citiesB :: CityBase
citiesB = [("anna@nasa.gov", "Washington")]


