import Data.Set (Set, singleton, union, empty, delete, insert, disjoint)

-----------------------------------------------------------
-- | Simple lambda terms
-----------------------------------------------------------

data Term
    = Var String
    | App Term Term
    | Abs String Term
    deriving (Show, Eq)

pretty :: Term -> String
pretty = prettyE . emb
    where
        emb (Var str) = EVar str
        emb (App t1 t2) = EApp (emb t1) (emb t2)
        emb (Abs x t) = EAbs x (emb t)

-- λf.λx. f^n x
churchEnc :: Integer -> Term
churchEnc n = Abs "f" (Abs "x" (c n))
  where
    c :: Integer -> Term
    c k | k < 1 = Var "x"
        | otherwise = App (Var "f") (c (k-1))

--λn.λf.λx. f (n f x)
churchSucc :: Term
churchSucc = Abs "n" $ Abs "f" $ Abs "x"
    (App (v "f") (App (App (v "n") (v "f")) (v "x")))
    where
        v = Var

term
    :: (String -> a)
    -> (a -> a -> a)
    -> (String -> a -> a)
    -> Term
    -> a
term var app abst lTerm = case lTerm of
    Var str -> var str
    App t s -> app (re t) (re s)
    Abs str t -> abst str (re t)
    where
        re = term var app abst

freeVars :: Term -> Set String
freeVars = term singleton union delete

boundVars :: Term -> Set String
boundVars = term (const empty) union insert

-- | substitute A x B = A [x:=B]
subs :: Term -> String -> Term -> Term
subs a v b = case a of
    Var str | str == v  -> b
            | otherwise -> a
    App t1 t2 -> App (subs t1 v b) (subs t2 v b)
    Abs x t | x == v -> a
            | disjoint (freeVars b) (boundVars a) ->
                Abs x (subs t v b)
            | otherwise -> undefined

-----------------------------------------------------------
-- | Extended lambda terms
-----------------------------------------------------------

data ETerm
    = Add
    | N Integer
    | EVar String
    | EApp ETerm ETerm
    | EAbs String ETerm
    deriving Show

eterm
    :: a
    -> (Integer -> a)
    -> (String -> a)
    -> (a -> a -> a)
    -> (String -> a -> a)
    -> ETerm
    -> a
eterm add_ n evar eapp eabs term = case term of
    Add -> add_
    N x -> n x
    EVar str -> evar str
    EApp t s -> eapp (re t) (re s)
    EAbs str t -> eabs str (re t)
    where
        re = eterm add_ n evar eapp eabs


prettyE :: ETerm -> String
prettyE Add = "Add"
prettyE (N x) = show x
prettyE (EVar name) = name
prettyE (EApp t l) = "(" ++ (prettyE t) ++ " " ++ (prettyE l) ++ ")"
prettyE (EAbs x t) = "L" ++ x ++ "." ++ (prettyE t)

