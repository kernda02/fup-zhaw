-- Eine mehrstellige Funktion
max3 :: (Integer, Integer, Integer) -> Integer
max3 (x,y,z) 
    | x >= y && x >= z = x
    | y >= x && y >= z = y
    | otherwise = z

-- Eine mehrstellige und mehrwerige Funktion
rotate :: (a, b, c) -> (c, a, b)
rotate (a, b, c) = (c, a, b)

-- Eine anonyme Funktion (mit einem Namen ;-) )
mul3 :: Integer -> Integer
mul3 = \x -> 3 * x 

-- Wenn eine Funktion einfach ein Wert sein kann, dann können wir sie
-- auch in einer anderen Funktion zurückgeben
mul :: Integer -> Integer -> Integer
mul y = \x -> x * y

-- Jetzt gilt 'mul 3 == mul3'


-- Wenn eine Funktion einfach ein Wert sein kann, dann können wir sie
-- auch als Argument an eine andere Funktion übergeben:
zweimal :: (Integer -> Integer) -> (Integer -> Integer)
zweimal f x = f (f x)

mul9 = zweimal mul3

-- Currying von max3
max3a :: Integer -> (Integer, Integer) -> Integer
max3a x (y, z) = max3 (x,y,z)

max3b :: Integer -> Integer -> Integer -> Integer
max3b x y z = max3a x (y, z)

-- Kombinator von Funktionen
(>>>) :: (a -> b) -> (b -> c) -> (a -> c)
(>>>) f g x = g (f x)

mul12 = mul 4 >>> mul 3  

-- Mehrstellige Funktionen als Funktionen, die Listen verarbeiten.
max3L :: [Integer] -> Integer
max3L [x, y, z] = max3b x y z

maxNE :: [Integer] -> Integer
maxNE [x] = x
maxNE (x:xs) = max2 x (maxNE xs)
    where
        max2 x y
            | x < y     = y
            | otherwise = x

-- Listen
leereListe = []

geradeZahlenBis100 = [ 2 * i | i <- [0..50] ]

geradeZahlen = [2 * i | i <- [0..]]

--ganze :: [Integer]
ganze = [[x, y] | x <- [1..10], y <- [1..10]]

ganzeZahlen = 0 : concatMap (\i -> [i,-i]) [1..]

--primes :: Integer -> [Integer]
primes 0 = []
primes n = extend $ primes (n-1)

extend [] = [2]
extend l@(x:_) = f (x+1) l
    where
        f n [] = n:l
        f n (y:ys) 
            | n `mod` y == 0 = f (n+1) l
            | otherwise = f n ys 


-- Nichtleere Listen
data NEList a
    = Singleton a
    | NECons a (NEList a)

toList :: NEList a -> [a]
toList (Singleton a) = [a]
toList (NECons a as) = a:(toList as)

newtype Boxed a = Boxed a

newtype Stream a = Stream (a, (Stream a))

takeS 0 _ = []
takeS n (Stream (a, s)) = a : (takeS (n-1) s)

newtype RBoxed a = RBoxed {unbox :: a}