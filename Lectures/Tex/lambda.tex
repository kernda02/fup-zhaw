\documentclass[xcolor=dvipsnames]{beamer}

\input{header.tex}

\title{Funktionale Programmierung\\
    \small{$\lambda$ Kalkül} }
\date{}

\def\tabularxcolumn#1{m{#1}}
\setbeamertemplate{footline}[frame number]


\begin{document}

\maketitle


\begin{frame}{$\lambda$-Kalkül}{Einführung}
\textbf{Erinnerung:}
\begin{quote}
	Functional programming is […] evaluation of expressions, rather than execution of commands […]
\end{quote}

\begin{itemize}
	\item Sie kennen mathematische Modelle, die den prozeduralen Ansatz formalisieren (Registermaschinen, Turingmaschinen, Kellerautomaten, Protosprachen: WHILE, GOTO)
	\item Wie sieht eine funktionale ``Proto-Sprache'' aus?
\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalkül}{Einführung}
$\lambda$ Kalküle:
\begin{itemize}
	\item Implementieren die Idee von Algorithmen als ``evaluation of expressions''
	\item Einfache aber ausdrucksstarke Syntax
	\item Mächtige Semantik (Turing vollständig)
	\item Modelliert Funktionsdefinitionen (Abstraction) und Funktionsanwendung (Evaluation)
	\item Implementiert die ``programs as data duality'' (höhere Funktionen)
	\item Funktionale Sprachen können als Variante gesehen werden
\end{itemize}
\end{frame}


\defverbatim[colored]\rein{
	\begin{lstlisting}[frame=leftline ]
let f x = 3 * x
	\end{lstlisting}
}
\defverbatim[colored]\pfeilAss{
	\begin{lstlisting}[frame=leftline ]
f: int -> int -> int
	\end{lstlisting}
}

\begin{frame}{$\lambda$-Kalküle}{Einführung}
	Lambda Kalküle bestehen aus folgenden ``Zutaten'':
	\begin{itemize}
		\item Terme (``Programme'' im $\lambda$-Kalkül)
		\item Regeln zur Manipulation von Termen
		\item Eventuell ein Typensystem (typisierte Kalküle)
	\end{itemize}
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{Terme}
Der Zeichenvorrat (Alphabet) mit dem wir $\lambda$-Terme bauen wollen besteht aus unendliche vielen Variablen $x,y,z,v,v_1,x_1,..$, dem Zeichen $\lambda$, dem Punkt und Klammern. Fakultativ können dem Alphabet beliebig Konstanten hinzugefügt werden.

\hspace{0.5cm}

$\lambda$-Terme sind folgendermassen gegeben:
\begin{itemize}
	\item Jede Variable und jede Konstante ist ein Term.
	\item Sind $A$ und $B$ Terme, dann ist auch $(A\; B)$ ein Term (Applikation).
	\item Ist $x$ eine Variable und $A$ ein Term, dann ist auch $\lambda x. A$ ein Term (Abstraktion).
\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Terme}
	Bemerkungen:
	\begin{itemize}
		\item Ein Term von der Form $(A\; B)$ steht für die Anwendung von $A$ auf $B$. Wie z.B. in Haskell der Ausdruck \texttt{(f x)} für das Resultat der Anwendung von \texttt{f} auf \texttt{x} steht.
		\item Ein Term $\lambda x. A$ entspricht einer anonymen Funktion \texttt{\textbackslash x -> A}. Aufgrund der Typisierung von Haskell lassen sich nicht alle Terme (des untypisierten $\lambda$-Kalküls) in Haskell realisieren (Bsp.: $\lambda x.(x\; x)$).
	\end{itemize}
\end{frame}


\defverbatim[colored]\TermsEx{
	\begin{lstlisting}[frame=leftline ]
data Term
    = Var String
    | App Term Term
    | Abs String Term
	\end{lstlisting}
}

\begin{frame}{$\lambda$-Kalküle}{Terme}
	Implementieren Sie einen Typ \texttt{Term} entsprechend der Vorlage unten in F\#, der $\lambda$-Terme repräsentiert. Es sollen dabei folgende Vorgaben implementiert werden:
	\begin{itemize}
		\item Implementieren sie Variablen als beliebige Strings.
		\item Folgende Konstanten sollen implementiert werden:
		\begin{itemize}
			\item Zahlen (\texttt{int})
			\item Funktionsterme: \texttt{add}, \texttt{IsZero}
		\end{itemize}
	\end{itemize}
	\TermsEx
\end{frame}

\defverbatim[colored]\Terms{
	\begin{lstlisting}[frame=leftline ]
data ETerm
    = Add
    | N Integer
    | EVar String
    | EApp ETerm ETerm
    | EAbs String ETerm
	\end{lstlisting}
}


\begin{frame}{$\lambda$-Kalküle}{Terme}
	\Terms
\end{frame}


\begin{frame}[fragile]{$\lambda$-Kalküle}{Terme}
	\begin{ex}
		Implementieren Sie eine Funktion \texttt{pretty}, die Objekte vom Typ \texttt{ETerm} als String repräsentiert.
	\end{ex}
	Beispielausgabe für

	\begin{lstlisting}
EAbs "x" $
    EAbs "y" $
        EApp
            (EApp Add (EVar "x"))
            (EVar "y")
    \end{lstlisting}

	ist:

	\texttt{Lx.Ly.((Add x) y)}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Terme}
Einige Konventionen bezüglich der Schreibweise von Termen:
\begin{itemize}
	\item Äussere Klammern können weggelassen werden
	\begin{itemize}
		\item $A$ wird als $(A)$ gelesen.
	\end{itemize}
	\item Applikation ist linksassoziativ.
	\begin{itemize}
		\item $A\; B\; C$ wird als $((A\; B)\; C)$ gelesen.
	\end{itemize}
	\item Der Bereich einer ``quantifizierten Variable'' wird grösstmöglich angenommen.
	\begin{itemize}
		\item $\lambda x.A\; B\; C$ wird als $\lambda x.((A\; B)\; C)$ gelesen.
	\end{itemize}
	\item Terme von der Form $\lambda x y z. A$ werden als $\lambda x.\lambda y.\lambda z. A$ gelesen.
\end{itemize}
\begin{ex}
	Schreiben Sie den Term $\lambda x y. A B \lambda u. A $ aus.
\end{ex}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Freie und gebundene Variablen}
	Die Menge der freien Variablen $FV(A)$ eines $\lambda$-Terms $A$ ist wie folgt gegeben:
	\begin{itemize}
		\item Ist $A$ eine Konstante, dann ist $FV(A)=\varnothing$.
		\item Ist $A$ eine Variable $v$, dann gilt $FV(A) = \{v\}$.
		\item Ist $A=(B\; C)$, dann ist $FV(A)=FV(B)\cup FV(C)$.
		\item Ist $A=\lambda x.B$, dann ist $FV(A)=FV(B)\setminus \{x\}$.
	\end{itemize}
Variablen, die in einem Term vorkommen aber nicht frei sind heissen gebundene Variablen.
\end{frame}


\begin{frame}[fragile]{$\lambda$-Kalküle}{Freie und gebundene Variablen}
	\begin{ex}
		Implementieren Sie die Funktion
        \begin{lstlisting}
freeVars :: ETerm -> Set String
        \end{lstlisting}
        in Haskell.
	\end{ex}
	\begin{itemize}
		\item Testen Sie Ihre Funktion am Term $(y\; (\lambda x. Add))$.
	\end{itemize}
\end{frame}

\defverbatim[colored]\freeVar{
	\begin{lstlisting}[frame=leftline ]
freeVars :: ETerm -> Set String
freeVars = eterm empty (const empty) singleton union minus
    where
        minus str set = set \\ singleton str
	\end{lstlisting}
}

\begin{frame}{$\lambda$-Kalküle}{Freie und gebundene Variablen}
	\freeVar
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{Substitution}
	\begin{itemize}
		\item Den Term $A[x:=B]$ erhält man aus dem Term $A$, indem man alle freien Vorkommen der Variablen $x$ in $A$ durch den Term $B$ ersetzt.
		\item Eine Substitution $A[x:=B]$ ist zulässig, wenn keine der freien Variablen von $B$ durch die Substitution gebunden werden.
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Substitution}
	\begin{itemize}
		\item Eine unzulässige Substitution
		\begin{align*}
		\lambda x y. (x\; y\; z) [z := x]
		\end{align*}

		\item Eine zulässige Substitution
		\begin{align*}
		\lambda x y. (x\; y\; z) [z:=u]
		\end{align*}
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Substitution}
	Durch geeignetes Umbenennen von Variablen lässt sich stets zulässig substituieren (vgl. Praktikum). Wir wollen uns nun davon überzeugen, dass Rechnen im $\lambda$-Kalkül im wesentlichen durch Substitutionen erreicht werden kann.

	\begin{ex}
		Was denken Sie was das Resultat ist wenn wir den Term
		\begin{align*}
			(((\lambda f g x. (f \; (g\; x))\; (add\; 3))\; (add\; 2))\; 0)
		\end{align*}
		``ausrechnen''. (Lösung siehe Wandtafel)
	\end{ex}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Reduktionen}
	Wir haben gesehen, dass Terme ``ausrechnen'' im $\lambda$-Kalkül darauf beruht substitutionen vorzunehmen und damit Terme so lange zu vereinfachen, bis sie in einer möglichst einfachen Form vorliegen.
	\begin{itemize}
		\item Beim Vereinfachen eines Termes spricht man von einer ``Konversion''. Wir werden im folgenden vier Arten von Konversionen betrachten ($\alpha,\beta,\eta,\delta$).
		\item Das Ziel der Konversionen, einen ``nicht mehr zu vereinfachenden Term'' nennt man eine Normalform. Wir werden die $\beta-$Normalform betrachten.
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{$\alpha$-Reduktion}
	\begin{itemize}
		\item Ziel der $\alpha$-Konversion ist es ``äquivalente'' Terme wie $\lambda x.x$ und $\lambda y.y$ ineinander überführen zu können.
		\item Die $\alpha$-Konversion formalisiert die Idee, dass die Bedeutung eines Terms nicht von den verwendeten Variablen sondern nur von seiner Struktur abhängt.
	\end{itemize}
\begin{definition}
	Die $\alpha$-Konversion:
\begin{align*}
\lambda x.A \Rightarrow_\alpha \lambda y.A[x:=y]
\end{align*}
wenn $y$ nicht in $A$ vorkommt.
\end{definition}
Beispiel:
\begin{align*}
\lambda fgx.(g\;f\;x) \Rightarrow_\alpha \lambda hgx.(g\;h\;x)
\end{align*}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{$\beta$-Reduktion}
	\begin{itemize}
		\item Die $\beta$-Konversion formalisiert die Idee der Funktionsanwendung durch ersetzen von Werten durch entsprechende Funktionswerte.

	\end{itemize}
	\begin{definition}
		Die $\beta$-Konversion:
		\begin{align*}
		(\lambda x.A\; B)\Rightarrow_\beta A[x:=B]
		\end{align*}
		wobei die Substitution zulässig sein muss.
	\end{definition}
	Beispiel:
	\begin{align*}
	(\lambda x.(Add\; x\; x)\; z)\Rightarrow_\beta (Add \; z\; z)
	\end{align*}
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{$\eta$-Reduktion}
	\begin{itemize}
		\item Die $\eta$-Konversion formalisiert die Idee, dass Funktionen, die dieselben Rückgabewerte produzieren gleich sind.


	\end{itemize}
	\begin{definition}
		Die $\eta$-Konversion:
		\begin{align*}
		(\lambda x.A\; x)\Rightarrow_\eta A
		\end{align*}
		wobei $x\notin FV(A)$ gelten muss und $A$ keine Konstante ist, die nicht eine Funktion darstellt.

	\end{definition}
	Beispiel:
	\begin{align*}
	\lambda x.(Add\; y\; x)\Rightarrow_\eta (Add \; y)
	\end{align*}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{$\delta$-Konversion}
\begin{itemize}
	\item 	In $\lambda$-Kalkülen mit Konstanten bestimmen $\delta$-Konversionen die Bedeutung der Konstanten.
	\item Beispiel:
	\begin{align*}
	(Add\; 14)\; 3 \Rightarrow_\delta 17
	\end{align*}
\end{itemize}

\end{frame}


\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
	\begin{align*}
		\lambda f.\lambda x. (f\; (f\; x)) \; (Add\; 3)\; 2\\
		\Rightarrow_\beta \lambda x. ((Add \; 3) \; ((Add\; 3)\; x)) \; 2\\
		\Rightarrow_\beta ((Add \; 3) \; ((Add\; 3)\; 2))\\
		\Rightarrow_\delta ((Add\; 3)\; 5)\\
		\Rightarrow_\delta 8
	\end{align*}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
\begin{itemize}
	\item Den Übergang von einem Term der Form $(\lambda x.A\; B)$ zu $A[x:=B]$ mittels $\beta$-Konversion nennen wir eine $\beta$-Reduktion.
	\item Einen Term auf den wir eine $\beta$-Reduktion anwenden können nennen wir einen $\beta$ Redex.
	\item Den Übergang von einem Term $A$ in einen Term $B$ durch $\delta$-Koversion, wobei in $B$ der Wert der entsprechenden Konstante eingesetzt wurde, nennen wir eine $\delta$-Reduktion.
	\item Ein Term ist in $\beta$-Normalform, wenn er keinen $\beta$-Redex als Teilterm enthält und keine $\delta$-Reduktion mehr möglich ist. (d.h. im Term ist keine $\beta$- oder $\delta$-Reduktion mehr möglich).
	\item Ein Term $A$ evaluiert zum Term $B$, wenn $B$ in $\beta$-Normalform ist und eine endliche Sequenz von Reduktionen von $A$ nach $B$ existiert.
\end{itemize}
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
	Die Anzahl der Redexe verringert sich mit einer Reduktion nicht notwendigerweise:
	\begin{align*}
	\lambda f.(f\; f\; f)\; (\lambda x.A)\\
	\Rightarrow_\beta (\lambda x.A)\; (\lambda x.A)\; (\lambda x.A)
	\end{align*}
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
	Nicht jeder Term kann zu einer $\beta$-Normalform reduziert werden:
	\begin{align*}
	\lambda x.(x\; x)\; \lambda x.(x\; x)
	\end{align*}
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
	Die Reihenfolge in der Reduktionen angewendet werden können ist im Allgemeinen nicht eindeutig:
	\begin{itemize}
		\item Reduktion ``von innen nach aussen'':
		\begin{align*}
			\lambda x.y \; (\lambda w. (w\; w)\; \lambda x.x )\\
			\Rightarrow_\beta \lambda x.y \; (\lambda x.x \; \lambda x.x )\\
			\Rightarrow_\beta \lambda x.y \; \lambda x.x\\
			\Rightarrow_\beta y
		\end{align*}
		\item Reduktion ``von Links nach Rechts'':
		\begin{align*}
		\lambda x.y \; (\lambda w. (w\; w)\; \lambda x.x )\\
		\Rightarrow_\beta y
		\end{align*}
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
	Evaluationsstrategien:
	\begin{itemize}
		\item ``Normal order reduction'' entspricht der Reduktionsstrategie ``von Links nach Rechts''; der jeweils am weitesten links stehende Redex wird evaluiert. Lazy evaluation ist eine Variante dieser Strategie.
		\item ``Applicative order reduction'' entspricht der Reduktionsstrategie ``von innen nach aussen''; der jeweils innerste Redex wird zuerst reduziert\footnote{Die Argumente einer Funktion werden ausgewertet bevor die Funktion selbst ausgewertet wird.}. Strict evaluation ist eine Variante dieser Strategie.
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}
	\begin{itemize}
		\item Wenn ein Term eine $\beta$-Normalform besitzt, dann wird diese immer durch ``normal order reduction'' gefunden.
		\item Es gibt Terme, die eine $\beta$-Normalform besitzen, die nicht mit ``applicative order reduction'' gefunden werden kann.
		\item Unabhängig von der gewählten Evaluationsstrategie, ist die (falls vorhanden) erhaltene $\beta$-Normalform bis auf $\alpha$-Konversion eindeutig.
	\end{itemize}
\end{frame}

\begin{frame}{$\lambda$-Kalküle}{``Rechnen'' mit Konversionen}

	Ein Term, der mit ``normal order reduction'' nicht aber mit ``applicative order reduction'' reduziert werden kann:
	\begin{itemize}
		\item Applicative order:
		\begin{align*}
			\lambda x. y \;(\lambda z. (z\; z\; z)\; \lambda z. (z\; z\; z) )\\
			\Rightarrow_\beta \lambda x. y \; (\lambda z. (z\; z\; z)\;\lambda z. (z\; z\; z)\; \lambda z. (z\; z\; z) )\\
			\Rightarrow_\beta \dots
		\end{align*}
		\item Normal order reduction:
				\begin{align*}
				\lambda x. y  \; (\lambda z. (z\; z\; z)\; \lambda z. (z\; z\; z) )\\
				\Rightarrow_\beta y
				\end{align*}
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Vollständigkeit}
	\begin{itemize}
		\item Der $\lambda$-Kalkül beinhaltet keine ``expliziten'' Konstrukte um rekursiv Funktionen zu deklarieren.
		\item Rekursion wird über Fixpunkte erreicht.
		\item Der Fixpunktkombinator $Y$ (entspricht im wesentlichen \texttt{fix}) kann im untypisierten $\lambda$-Kalkül direkt als Term geschrieben werden:
		\begin{align*}
			Y:\equiv \lambda f.(\lambda x.(f\ (x\; x))\; \lambda x.(f\; (x\; x)))
		\end{align*}
	\end{itemize}
\end{frame}


\begin{frame}{$\lambda$-Kalküle}{Vollständigkeit}
	Die Fixpunkteigenschaft $(Y\; g) = (g \; (Y\; g))$ kann einfach überprüft werden:
	\begin{align*}
		(Y\; g) =_{Def.} \lambda f.(\lambda x.(f\ (x\; x))\; \lambda x.(f\; (x\; x)))\; g\\
		\Rightarrow_\beta (\lambda x.(g\ (x\; x))\; \lambda x.(g\; (x\; x)))\\
		\Rightarrow_\beta g\; (\lambda x.(g\; (x\; x)) \; \lambda x.(g\; (x\; x)))\\
		\Rightarrow_\beta g\; (\lambda f.(\lambda x.(f\; (x\; x)) \; \lambda x.(f\; (x\; x)))\;g)\\
		=_{Def} g\; (Y\; g)
	\end{align*}
\end{frame}



\begin{frame}{$\lambda$-Kalküle}{Vollständigkeit}
	Weitere gängige Konstrukte und Datentypen der Programmierung lassen sich im $\lambda$-Kalkül simulieren:
	\begin{itemize}
		\item While Schleifen via Rekursion (mittels Fixpunkten).
		\item Natürliche Zahlen (durch ``Church Numerale'').
		\item Paare und deren Projektionen (mittels expliziter Paarungsfunktion).
		\item Listen (via Paare).
		\item \dots
	\end{itemize}
	Folgerung: Der $\lambda$-Kalkül ist Turing-vollständig.
\end{frame}





\end{document}