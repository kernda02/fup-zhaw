\documentclass[xcolor=dvipsnames
    , handout
    ]{beamer}

\input{header.tex}

\title{Funktionale Programmierung\\
    \small{Repetition Teil 1} }
\date{}

\def\tabularxcolumn#1{m{#1}}
\setbeamertemplate{footline}[frame number]


\begin{document}

\maketitle

\begin{fr}
    \tableofcontents
\end{fr}

\section{Funktionen}

\subsection*{Definieren einer Funktion}

\begin{fr}[fragile]
Simple einstellige Funktion.

\begin{itemize}
\item ``Normale''/mathematische Notation mit Klammern um das Argument:
\begin{align*}
f(x) = 3\cdot x
\end{align*}
\item Haskell Syntax ist ohne die Klammern:
\begin{lstlisting}[frame=leftline]
f x = 3 * x
\end{lstlisting}
\end{itemize}
\end{fr}

\subsection*{Verzweigungen/Fallunterscheidung}
\begin{fr}[fragile]
Wie in der Mathematik oder in anderen Programmiersprachen üblich, lassen sich Verzweigungen
/Fallunterscheidungen mit einem If-Else Konstrukt realisieren:
\begin{itemize}
\item Mathematisch:
\begin{align*}
next(x) =
    \begin{cases}
        \frac{x}{2}&\text{falls $x$ gerade}\\
        3 x + 1&\text{sonst}
    \end{cases}
\end{align*}
    \item Haskell (guard)
\begin{lstlisting}
next x
    | x `mod` 2 == 0 = x `div` 2
    | otherwise = 3 * x + 1
\end{lstlisting}
\end{itemize}
\end{fr}

\begin{fr}[fragile]

\begin{itemize}

\item Haskell (if then else)
\begin{lstlisting}
next x = if x `mod` 2 == 0
    then
        x `div` 2
    else
        3 * x + 1
\end{lstlisting}
\end{itemize}
\begin{rk}
    If-Else Konstrukte entsprechen in Haskell ``normalen Werten''. Der Ausdruck
    \texttt{if True then 17 else 0} ist als Beschreibung der Zahl $17$, gleichwertig mit z.B. \texttt{10 + 7} oder \texttt{17}. Daraus folgt insbesondere, dass in einem If-Else alle Optionen vom gleichen Typ sein müssen.
\end{rk}
\end{fr}


\subsection*{Die Signatur einer einstelligen Funktion}
\begin{fr}[fragile]
\begin{itemize}
    \item Eine Funktion (z.B. $f(x)= 3\cdot x$) nimmt eine Eingabe entgegen und retourniert dann eine entsprechende Ausgabe.
    \item Damit die Funktion $f$ wirklich eindeutig definiert ist, müssen wir festlegen welche Eingaben in
    Frage kommen und welche Ausgaben sich daraus ergeben können.
\end{itemize}
\end{fr}

\begin{fr}[fragile]
Normalerweise werden die möglichen Eingabe- und Ausgabewerte einer Funktion durch den Definitionsbereich und einen
Wertebereich der Funktion angegeben. Im Fall der Funktion $f(x)= 3x$ könnte dies z.B. durch die \textit{Signatur}
\begin{align*}
    f:\mathbb{Z}\to\mathbb{Z}
\end{align*}
gegeben sein. Die vollständige Definition der Funktion sähe also so aus:
\begin{align*}
    &f:\mathbb{Z}\to\mathbb{Z}\\
    &f(x)=3\cdot x
\end{align*}
\end{fr}

\begin{fr}[fragile]
In Haskell werden diese Einschränkungen mit \textit{Typen} (z.B. Bool, Int, String, etc.) notiert:
\begin{lstlisting}[frame=leftline]
f :: Integer -> Integer
f x = 3 * x
\end{lstlisting}
\end{fr}

\begin{fr}[fragile]
Wenn man in einer Funktionsdeklaration die Signatur weglässt, dann versucht Haskell eine möglichst
allgemeine Signatur herzuleiten (das nennt man ``Typ inferenz''). Bei der Funktion \texttt{f x = x * 3} wäre dies
die Signatur (der Typ)
\begin{align*}
\texttt{Num a => a -> a}
\end{align*}
Dies ist so zu verstehen, dass die Funktion auf einen beliebigen Wert angewendet werden kann, mit dem man ``rechnen
kann'' (das ist die Einschränkung auf \texttt{Num a}).
\end{fr}

\subsection*{Mehrstellige Funktionen}
\begin{fr}[fragile]
    Bei einer mehrstellige Funktion hängt der Rückgabewert nicht von einem sondern von mehreren Eingabewerten ab.
    Ein Besipiel einer mehrstelligen Funktion ist z.B.
    \begin{align*}
    max_3(x,y,z) =
    \begin{cases}
    x &\text{falls } x\geq y \land x\geq z\\
    y &\text{falls } y\geq x \land y\geq z\\
    z &\text{sonst }
    \end{cases}
    \end{align*}
    Dies läst sich in Haskell analog deklarieren:
    \begin{lstlisting}
max3 (x,y,z)
    | x >= y && x >= z = x
    | y >= x && y >= z = y
    | otherwise        = z
\end{lstlisting}
\end{fr}

\begin{fr}[fragile]
    Die Tatsache, dass es sich um eine mehrstellige Funktion handelt ist in der Signatur der Funktion sichtbar (möglicher Typ):
    \begin{lstlisting}[frame=leftline]
max3 :: (Integer, Integer, Integer) -> Integer
    \end{lstlisting}

\begin{ex}
    Implementieren Sie eine Funktion mit der Signatur.
    \begin{lstlisting}
f :: (a,b) -> (b,a,b)
    \end{lstlisting}
\end{ex}
\end{fr}

\subsection*{Anonyme Funktionen}

\begin{fr}[fragile]
    Wir können Funktionen auch direkt als Terme hinschreiben:
    \begin{lstlisting}[frame=leftline]
-- Eine anonyme Funktion (mit einem Namen ;-) )
mul3 :: Integer -> Integer
mul3 = \x -> 3 * x
    \end{lstlisting}
\end{fr}

\subsection*{Höhere Funktionen}

\begin{fr}[fragile]
Wenn eine Funktion einfach ein Wert sein kann, dann können wir sie
auch in einer anderen Funktion zurückgeben:
\begin{lstlisting}[frame=leftline]
-- mul 3 == mul3
mul :: Integer -> (Integer -> Integer)
mul x = \y -> x * y
\end{lstlisting}

Diese Deklaration ist im übrigen äquivalent zu
\begin{lstlisting}[frame=leftline]
mul :: Integer -> (Integer -> Integer)
mul x y = x * y
\end{lstlisting}
\end{fr}


\begin{fr}[fragile]
    Wenn eine Funktion einfach ein Wert sein kann, dann können wir sie
    auch als Argument an eine andere Funktion übergeben:
    \begin{lstlisting}[frame=leftline]
-- mul9 == mul 9
zweimal :: (a -> a) -> (a -> a)
zweimal f x = f (f x)

mul9 = zweimal mul3
    \end{lstlisting}

oder verschiedene Funktionen in einer Funktion ``kombinieren''
    \begin{lstlisting}[frame=leftline]
(>>) :: (a -> b) -> (b -> c) -> (a -> c)
(>>) f g x = g (f x)

mul12 = mul 4 >> mul 3
    \end{lstlisting}
\end{fr}

\begin{fr}[fragile]
    \begin{ex}
\begin{itemize}
    \item Implementieren Sie die Funktion \texttt{max3} als einstellige Funktion, die eine zweistellige Funktion zurückgibt.
    \item Implementieren Sie die Funktion \texttt{max3} als einstellige Funktion, die eine
    einstellige Funktion zurückgibt, die eine einstellige Funktion zurückgibt.
\end{itemize}
Es werden folgende Signaturen erwartet:
\begin{lstlisting}[frame=leftline]
-- (a)
max3a :: I -> ((I, I) -> I)
-- (b)
max3b :: I -> (I -> (I -> I))
\end{lstlisting}
wobei \texttt{I} für \texttt{Integer} stehe.
\end{ex}
\end{fr}

\section{Listen}

\subsection*{Listen definieren}

\begin{fr}[fragile]
    \begin{itemize}
        \item Explizit
        \begin{lstlisting}[frame = leftline]
[] -- leere Liste
[1,2,3]
["a", "b", "c"]
        \end{lstlisting}
        \item ``Angedeutet''
        \begin{lstlisting}[frame = leftline]
[1..100]
['a'..'z']
[1..] -- unendlich!
        \end{lstlisting}
        \item Comprehensions
        \begin{lstlisting}[frame = leftline]
[2 * x | x <- [1..10]]
[(x,y) | x <- [1..5], y <- [1..x]]
[1..] -- unendlich!
        \end{lstlisting}
    \end{itemize}
\end{fr}

\begin{fr}[fragile]
    \begin{ex}
\begin{itemize}
    \item Definieren Sie eine Liste vom Typ \texttt{[Integer]}, die alle ungeraden Zahlen zwischen $0$ und $100$ enthält.
    \item Definieren Sie eine Liste vom Typ \texttt{[Integer]}, die alle Quadratzahlen enthält.
\end{itemize}
    \end{ex}
\end{fr}

\subsection*{Funktionen auf Listen}

\begin{fr}[fragile]
\url{http://hackage.haskell.org/package/base-4.12.0.0/docs/Data-List.html}
\begin{ex}
    \begin{itemize}
        \item Benutzen Sie die Funktion \texttt{concatMap} um die Liste aller ganzen Zahlen zu erzeugen.
        \item Implementieren Sie eine Funktion
        \begin{lstlisting}[frame=leftline]
primes :: Integer -> [Integer]
        \end{lstlisting}
        die bei gegebenem $n$ die ersten $n$ Primzahlen zurückgibt.
    \end{itemize}
\end{ex}
\end{fr}

\begin{fr}[fragile]
Eine ``Erweiterung'' von mehrstelligen Funktionen sind solche, die beliebig viele Parameter verarbeiten können. Dies können wir mit (einstelligen) Funktionen modellieren, die (eine) Listen verarbeiten.
    \begin{lstlisting}[frame=leftline]
max3L :: [Integer] -> Integer
max3L [a,b,c] = max3 (a,b,c)
    \end{lstlisting}
\begin{ex}
    Diskutieren Sie die Definition von \texttt{max3L}.
\end{ex}
\end{fr}

\begin{fr}[fragile]
    Oft brauchen wir Rekursion, um Funktionen auf Listen für alle möglichen Eingaben zu definieren:
    \begin{lstlisting}[frame=leftline]
maxNE :: [Integer] -> Integer
maxNE [x] = x
maxNE (x:xs) = max2 x (maxL xs)
    where
        max2 x y
            | x < y     = y
            | otherwise = x
    \end{lstlisting}
\begin{ex}
    Diskutieren Sie die Definition von \texttt{maxNE}.
\end{ex}
\end{fr}

\section{Eigene Typen}
\subsection*{Unions}
\begin{fr}[fragile]
    Die Funktion, die wir vorher gesehen haben ist nicht auf beliebigen Listen, sondern auf ``nichtleeren'' Listen definiert. Wir können diesen Datentyp wie folgt als ``union-type'' definieren:
    \begin{lstlisting}[frame = leftline]
data NEList a
    = Singleton a
    | NECons a (NEList a)
    \end{lstlisting}
    Natürlich lassen sich unsere nichtleeren Listen in normale Listen umwandeln (aber nicht umgekehrt).
\begin{lstlisting}[frame=leftline]
toList :: NEList a -> [a]
toList (Singleton a) = [a]
toList (NECons a as) = a:(toList as)
\end{lstlisting}
\end{fr}

\begin{fr}[fragile]
    \begin{ex}
        Implementieren Sie die Funktion
        \begin{lstlisting}
maxNE :: NEList Integer -> Integer
        \end{lstlisting}
    \end{ex}
\end{fr}

\begin{fr}[fragile]
    \begin{itemize}
        \item Ein union type mit nur einem Konstruktor mit nur einem Datenfeld, können wir auch mit dem keyword \texttt{newtype} definieren:
        \begin{lstlisting}
newtype Boxed a = Boxed a
newtype Stream a = Stream (a, (Stream a))
        \end{lstlisting}
    \end{itemize}
\end{fr}

\subsection*{Records}
\begin{fr}[fragile]
Ein Record ist ein Tupel in dem die einzelnen Einträge mit konkreten Namen anstelle von indices referenziert werden:
\begin{lstlisting}[frame=leftline]
data Point = Point
    { x :: Float
    , y :: Float
    }
\end{lstlisting}
Jeder Eintrag definiert eine Funktion (``getter''):
\begin{lstlisting}[frame=leftline]
x :: Point -> Float
y :: Point -> Float
\end{lstlisting}
\end{fr}

\begin{fr}[fragile]
    \begin{itemize}
        \item Die Record Schreibweise kann \texttt{newtype} Keyword Kombiniert werden:
        \begin{lstlisting}
newtype Boxed a = Boxed {unbox :: a}
        \end{lstlisting}
    \end{itemize}
\end{fr}

\end{document}
